﻿using System;
using System.Windows.Forms;
using System.Data.OleDb;


namespace HotelRetailSupportMngment.v2
{

    public partial class StaffLogin : Form
    {
        public Form RefToForm1 { get; set; }
        public static string userName = "";
        private OleDbConnection connect = new OleDbConnection();
        public StaffLogin()
        {
            InitializeComponent();
            connect.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=E:\Documents\Visual Studio 2015\Projects\HotelRetailSupportMngment.v2\HotelRetailSupportMngment.v2\Resources\retailSupport.accdb";
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text == "" || txtUsername.Text == "")
            {
                MessageBox.Show("Please fill in Username and Password");
                return;
            }
            else
            {
                try
                {
                    connect.Open();
                    OleDbCommand command = new OleDbCommand();
                    command.Connection = connect;
                    command.CommandText = "select userName,Password,UserLevel from tbl_User where userName='" + txtUsername.Text + "' and Password='" + txtPassword.Text + "' and UserLevel='" + lbUserLevel.Text + "'";
                    OleDbDataReader reader = command.ExecuteReader();

                    int count = 0;
                    while (reader.Read())
                    {
                        count = count + 1;
                    }
                    //If count is equal to 1, than show AdminMenu form
                    if (count == 1)
                    {
                        MessageBox.Show("Login Successful!");
                        this.Hide();
                        connect.Close();
                        connect.Dispose();
                        userName = txtUsername.Text;

                        StaffMenu staffmenu = new StaffMenu();
                        staffmenu.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Username or password is incorrect!");
                    }
                    connect.Close();
                }
                catch (Exception a)
                {
                    MessageBox.Show("error" + a);
                }
            }
            
        }

        private void btnBack_Click(object sender, EventArgs e)
        {          
            this.RefToForm1.Show();
            this.Close();
        }

        private void StaffLogin_Load(object sender, EventArgs e)
        {

        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to exit?", "System Exit...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
