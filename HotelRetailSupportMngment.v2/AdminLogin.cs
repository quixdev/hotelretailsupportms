﻿using System;
using System.Windows.Forms;
using System.Data.OleDb;


namespace HotelRetailSupportMngment.v2
{
    public partial class AdminLogin : Form
    {
        private OleDbConnection connect = new OleDbConnection();
        public static string userName = "";
        public AdminLogin()
        {
            InitializeComponent();
            connect.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=E:\HotelRetailSupportMngment.v2\HotelRetailSupportMngment.v2\Resources\retailSupport.accdb";
        }
        public Form RefToForm1 { get; set; }

        private void AdminLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
        }
        private void AdminLogin_Load(object sender, EventArgs e)
        {
            try
            {                        
                connect.Open();
                chkConnection.Text = "Connected to Database";
                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error"+ ex);
            }
            
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text == "" || txtUsername.Text == "")
            {
                MessageBox.Show("Please fill in Username and Password");
                return;
            }
            else
            {
                try
                {
                    connect.Open();
                    OleDbCommand command = new OleDbCommand();
                    command.Connection = connect;
                    command.CommandText = "select userName,Password,UserLevel from tbl_User where userName='" + txtUsername.Text + "' and Password='" + txtPassword.Text + "' and UserLevel='" + lbUserLevel.Text + "'";
                    OleDbDataReader reader = command.ExecuteReader();

                    int count = 0;
                    while (reader.Read())
                    {
                        count = count + 1;
                    }
                    //If count is equal to 1, than show AdminMenu form
                    if (count == 1)
                    {
                        MessageBox.Show("Login Successful!");
                        this.Hide();
                        connect.Close();
                        connect.Dispose();
                        userName = txtUsername.Text;
                        AdminMenu adminmenu = new AdminMenu();
                        adminmenu.Show();
                    }
                    else
                    {
                        MessageBox.Show("Login Failed!");
                    }
                    connect.Close();
                }
                catch (Exception a)
                {
                    MessageBox.Show("error" + a);
                }
            }
            
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginMenu newloginMenu = new LoginMenu();
            newloginMenu.Visible = true;               
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to exit?", "System Exit...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void Logout_Click(object sender, EventArgs e)
        {

        }
    }
}
