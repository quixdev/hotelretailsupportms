﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HotelRetailSupportMngment.v2
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class RetailSupportDatabaseEntities : DbContext
    {
        public RetailSupportDatabaseEntities()
            : base("name=RetailSupportDatabaseEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tbl_Product> tbl_Product { get; set; }
        public virtual DbSet<tbl_purchaseOrder> tbl_purchaseOrder { get; set; }
        public virtual DbSet<tbl_Supplier> tbl_Supplier { get; set; }
        public virtual DbSet<tbl_User> tbl_User { get; set; }
    }
}
