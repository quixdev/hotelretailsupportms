﻿using System;
using System.Windows.Forms;
using System.Data.OleDb;

namespace HotelRetailSupportMngment.v2
{
    public partial class StaffProductForm : Form
    {
        private OleDbConnection connect = new OleDbConnection();
        public Form RefToFormStaffMenu { get; set; }
        public StaffProductForm()
        {
            InitializeComponent();
            connect.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=E:\Documents\Visual Studio 2015\Projects\HotelRetailSupportMngment.v2\HotelRetailSupportMngment.v2\Resources\retailSupport.accdb";
        }
       
        private void StaffProductForm_Load(object sender, EventArgs e)
        {            
            using (OleDbCommand command = new OleDbCommand())
            {
                connect.Open();
                command.Connection = connect;
                string query = "Select * from tbl_Supplier";
                command.CommandText = query;
                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    cboSupplierName.Items.Add(reader["supplierName"].ToString());
                }
                connect.Close();
            }

            using (OleDbCommand command1 = new OleDbCommand())
            {
                connect.Open();
                command1.Connection = connect;
                string query = "Select * from tbl_Product";
                command1.CommandText = query;
                OleDbDataReader reader1 = command1.ExecuteReader();
                while (reader1.Read())
                {
                    cboSearchProduct.Items.Add(reader1["productName"].ToString());
                }
                connect.Close();
            }
            
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.RefToFormStaffMenu.Show();
            this.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to Add data?", "System adding...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                if (txtProductName.Text == "" || txtProductQuantity.Text == "" || txtProductPrice.Text == "" || cboSupplierName.Text == "" || txtMinQuantity.Text == "")
                {
                    MessageBox.Show("Please fill in all the field");
                    return;
                }
                else
                {
                    try
                    {
                        //Query for add new data
                        connect.Open();
                        OleDbCommand command = new OleDbCommand();
                        command.Connection = connect;
                        command.CommandText = "insert into tbl_Product(productName,productQuantity,productPrice,supplierName,minimumQty) values ('" + txtProductName.Text + "','" + txtProductQuantity.Text + "','" + txtProductPrice.Text + "','" + cboSupplierName.Text + "','" + txtMinQuantity.Text + "')";
                        command.ExecuteNonQuery();
                        MessageBox.Show("Data Saved into tbl_Product");
                        connect.Close();

                        cboSearchProduct.Enabled = true;
                        resetting();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("error" + ex);
                        connect.Close();
                    }
                }
            }
               
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to save data?", "System saving...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                if (cboSearchProduct.Text == "" || txtProductQuantity.Text == "" || txtProductPrice.Text == "" || cboSupplierName.Text == "" || txtMinQuantity.Text == "")
                {
                    MessageBox.Show("Please fill in all the field");
                    return;
                }
                else
                {
                    try
                    {
                        //Query For Edit data based on ProductName
                        connect.Open();
                        OleDbCommand command = new OleDbCommand();
                        command.Connection = connect;
                        string query = "update tbl_Product set productName ='" + cboSearchProduct.Text + "',productPrice ='" + txtProductPrice.Text + "' ,productQuantity ='" + txtProductQuantity.Text + "' ,supplierName ='" + cboSupplierName.Text + "' ,minimumQty ='" + txtMinQuantity.Text + "' where productName='" + cboSearchProduct.Text + "'";
                        MessageBox.Show(query);
                        command.CommandText = query;
                        command.ExecuteNonQuery();
                        MessageBox.Show("Data Updated/Edit & Saved into tbl_Product");
                        connect.Close();

                        //System Control Input
                        txtProductName.Enabled = true;
                        resetting();
                        btnAdd.Enabled = true;
                        btnDelete.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("error" + ex);
                        connect.Close();
                    }
                }
            }
                
        }

        private void cboSearchProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //Query For Edit data based on ProductName
                connect.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connect;
                string query = "select * from tbl_Product where ProductName='"+cboSearchProduct.Text+"'";
                command.CommandText = query;
                OleDbDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    txtProductName.Text = reader["productName"].ToString();
                    txtProductPrice.Text = reader["productPrice"].ToString();
                    txtProductQuantity.Text = reader["productQuantity"].ToString();
                    cboSupplierName.Text = reader["SupplierName"].ToString();
                    txtMinQuantity.Text = reader["minimumQty"].ToString();
                }
                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
                connect.Close();
            }
            txtProductName.Enabled = false;
            btnAdd.Enabled = false;
          
        }
        public void resetting()
        {
            txtProductName.Clear();
            txtMinQuantity.Clear();
            txtProductQuantity.Clear();
            txtProductPrice.Clear();
            cboSupplierName.SelectedIndex = -1;
            cboSearchProduct.SelectedIndex = -1;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to delete data?", "System deleting...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                try
                {
                    //Query For Delete data by ProductName
                    connect.Open();
                    OleDbCommand command = new OleDbCommand();
                    command.Connection = connect;
                    string query = "delete from tbl_Product where productName='" + cboSearchProduct.Text + "'";
                    MessageBox.Show(query);
                    command.CommandText = query;
                    command.ExecuteNonQuery();
                    MessageBox.Show("Data Removed from tbl_Product");
                    connect.Close();

                    //System Control Input
                    txtProductName.Enabled = true;
                    resetting();
                    btnAdd.Enabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error" + ex);
                    connect.Close();
                }
            }
               
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to exit?", "System Exit...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void Logout_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginMenu newloginMenu = new LoginMenu();
            newloginMenu.Visible = true;
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
