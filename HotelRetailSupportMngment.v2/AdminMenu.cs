﻿using System;
using System.Windows.Forms;

namespace HotelRetailSupportMngment.v2
{
    public partial class AdminMenu : Form
    {
        public AdminMenu()
        {
            InitializeComponent();
        }
        
        private void btnProduct_Click(object sender, EventArgs e)
        {
            AdminProductForm adminProduct = new AdminProductForm();
            adminProduct.RefToFormAdminMenu = this;
            this.Visible = false;
            adminProduct.Show();
        }

        private void btnInventory_Click(object sender, EventArgs e)
        {
            PurchaseOrderInventory adminInventory = new PurchaseOrderInventory();
            adminInventory.RefToFormAdminMenu = this;
            this.Visible = false;
            adminInventory.Show();
        }

        private void btnSupplier_Click(object sender, EventArgs e)
        {
            AdminSupplierForm adminSupplier = new AdminSupplierForm();
            adminSupplier.RefToFormAdminMenu = this;
            this.Visible = false;
            adminSupplier.Show();
        }

        private void btnUser_Click(object sender, EventArgs e)
        {
            AdminUserForm adminUser = new AdminUserForm();
            adminUser.RefToFormAdminMenu = this;
            this.Visible = false;
            adminUser.Show();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginMenu newloginMenu = new LoginMenu();
            newloginMenu.Visible = true;
        }

        private void AdminMenu_Load(object sender, EventArgs e)
        {

        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to exit?", "System Exit...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
