﻿namespace HotelRetailSupportMngment.v2
{
    partial class StaffInventoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label purchaseOrderDateLabel;
            System.Windows.Forms.Label purchaseOrderIDLabel1;
            System.Windows.Forms.Label productNameLabel;
            System.Windows.Forms.Label userNameLabel;
            System.Windows.Forms.Label orderQuantityLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StaffInventoryForm));
            this.label1 = new System.Windows.Forms.Label();
            this.cboProductName = new System.Windows.Forms.ComboBox();
            this.cboUserName = new System.Windows.Forms.ComboBox();
            this.orderQuantityNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.cboPurchaseOrderId = new System.Windows.Forms.ComboBox();
            this.cboSupplierName = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPurchaseDate = new System.Windows.Forms.TextBox();
            this.llbEnable = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.Logout = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            purchaseOrderDateLabel = new System.Windows.Forms.Label();
            purchaseOrderIDLabel1 = new System.Windows.Forms.Label();
            productNameLabel = new System.Windows.Forms.Label();
            userNameLabel = new System.Windows.Forms.Label();
            orderQuantityLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.orderQuantityNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // purchaseOrderDateLabel
            // 
            purchaseOrderDateLabel.AutoSize = true;
            purchaseOrderDateLabel.Location = new System.Drawing.Point(20, 80);
            purchaseOrderDateLabel.Name = "purchaseOrderDateLabel";
            purchaseOrderDateLabel.Size = new System.Drawing.Size(109, 13);
            purchaseOrderDateLabel.TabIndex = 8;
            purchaseOrderDateLabel.Text = "purchase Order Date:";
            // 
            // purchaseOrderIDLabel1
            // 
            purchaseOrderIDLabel1.AutoSize = true;
            purchaseOrderIDLabel1.Location = new System.Drawing.Point(32, 50);
            purchaseOrderIDLabel1.Name = "purchaseOrderIDLabel1";
            purchaseOrderIDLabel1.Size = new System.Drawing.Size(97, 13);
            purchaseOrderIDLabel1.TabIndex = 17;
            purchaseOrderIDLabel1.Text = "purchase Order ID:";
            // 
            // productNameLabel
            // 
            productNameLabel.AutoSize = true;
            productNameLabel.Location = new System.Drawing.Point(52, 105);
            productNameLabel.Name = "productNameLabel";
            productNameLabel.Size = new System.Drawing.Size(77, 13);
            productNameLabel.TabIndex = 18;
            productNameLabel.Text = "product Name:";
            // 
            // userNameLabel
            // 
            userNameLabel.AutoSize = true;
            userNameLabel.Location = new System.Drawing.Point(68, 132);
            userNameLabel.Name = "userNameLabel";
            userNameLabel.Size = new System.Drawing.Size(61, 13);
            userNameLabel.TabIndex = 19;
            userNameLabel.Text = "user Name:";
            // 
            // orderQuantityLabel
            // 
            orderQuantityLabel.AutoSize = true;
            orderQuantityLabel.Location = new System.Drawing.Point(50, 185);
            orderQuantityLabel.Name = "orderQuantityLabel";
            orderQuantityLabel.Size = new System.Drawing.Size(76, 13);
            orderQuantityLabel.TabIndex = 20;
            orderQuantityLabel.Text = "order Quantity:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(78, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(271, 30);
            this.label1.TabIndex = 5;
            this.label1.Text = "Inventory Transaction (Staff)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cboProductName
            // 
            this.cboProductName.FormattingEnabled = true;
            this.cboProductName.Location = new System.Drawing.Point(135, 102);
            this.cboProductName.Name = "cboProductName";
            this.cboProductName.Size = new System.Drawing.Size(200, 21);
            this.cboProductName.TabIndex = 2;
            // 
            // cboUserName
            // 
            this.cboUserName.FormattingEnabled = true;
            this.cboUserName.Location = new System.Drawing.Point(135, 129);
            this.cboUserName.Name = "cboUserName";
            this.cboUserName.Size = new System.Drawing.Size(200, 21);
            this.cboUserName.TabIndex = 4;
            // 
            // orderQuantityNumericUpDown
            // 
            this.orderQuantityNumericUpDown.Location = new System.Drawing.Point(135, 183);
            this.orderQuantityNumericUpDown.Name = "orderQuantityNumericUpDown";
            this.orderQuantityNumericUpDown.Size = new System.Drawing.Size(66, 20);
            this.orderQuantityNumericUpDown.TabIndex = 6;
            // 
            // cboPurchaseOrderId
            // 
            this.cboPurchaseOrderId.FormattingEnabled = true;
            this.cboPurchaseOrderId.Location = new System.Drawing.Point(135, 47);
            this.cboPurchaseOrderId.Name = "cboPurchaseOrderId";
            this.cboPurchaseOrderId.Size = new System.Drawing.Size(121, 21);
            this.cboPurchaseOrderId.TabIndex = 0;
            this.cboPurchaseOrderId.SelectedIndexChanged += new System.EventHandler(this.cboPurchaseOrderId_SelectedIndexChanged_1);
            // 
            // cboSupplierName
            // 
            this.cboSupplierName.FormattingEnabled = true;
            this.cboSupplierName.Location = new System.Drawing.Point(135, 156);
            this.cboSupplierName.Name = "cboSupplierName";
            this.cboSupplierName.Size = new System.Drawing.Size(200, 21);
            this.cboSupplierName.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Supplier Name:";
            // 
            // txtPurchaseDate
            // 
            this.txtPurchaseDate.Location = new System.Drawing.Point(135, 77);
            this.txtPurchaseDate.Name = "txtPurchaseDate";
            this.txtPurchaseDate.Size = new System.Drawing.Size(200, 20);
            this.txtPurchaseDate.TabIndex = 1;
            // 
            // llbEnable
            // 
            this.llbEnable.AutoSize = true;
            this.llbEnable.Location = new System.Drawing.Point(262, 50);
            this.llbEnable.Name = "llbEnable";
            this.llbEnable.Size = new System.Drawing.Size(42, 13);
            this.llbEnable.TabIndex = 31;
            this.llbEnable.TabStop = true;
            this.llbEnable.Text = "Update";
            this.llbEnable.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llbEnable_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 217);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(291, 39);
            this.label3.TabIndex = 32;
            this.label3.Text = "Instruction:\r\n1. Click on \"Update\" to search Purchase Order ID to update\r\n2. Date" +
    " format \"00-Sep-17\"";
            // 
            // Logout
            // 
            this.Logout.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Logout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Logout.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Logout.Image = global::HotelRetailSupportMngment.v2.Properties.Resources.icons8_Logout_Rounded_Down;
            this.Logout.Location = new System.Drawing.Point(241, 275);
            this.Logout.Name = "Logout";
            this.Logout.Size = new System.Drawing.Size(75, 23);
            this.Logout.TabIndex = 33;
            this.Logout.Text = "Logout";
            this.Logout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Logout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Logout.UseVisualStyleBackColor = false;
            this.Logout.Click += new System.EventHandler(this.Logout_Click);
            // 
            // Exit
            // 
            this.Exit.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Exit.Image = global::HotelRetailSupportMngment.v2.Properties.Resources.icons8_Exit_26;
            this.Exit.Location = new System.Drawing.Point(322, 267);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(87, 39);
            this.Exit.TabIndex = 34;
            this.Exit.Text = "Exit";
            this.Exit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Exit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Exit.UseVisualStyleBackColor = false;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Image = global::HotelRetailSupportMngment.v2.Properties.Resources.icons8_Back_Arrow;
            this.btnBack.Location = new System.Drawing.Point(28, 275);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 10;
            this.btnBack.Text = "Back";
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Image = global::HotelRetailSupportMngment.v2.Properties.Resources.icons8_Delete_Folder;
            this.btnDelete.Location = new System.Drawing.Point(341, 145);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 43);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Text = "Delete";
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdd.Image = global::HotelRetailSupportMngment.v2.Properties.Resources.icons8_Save_as;
            this.btnAdd.Location = new System.Drawing.Point(341, 96);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 43);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Add";
            this.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Image = global::HotelRetailSupportMngment.v2.Properties.Resources.icons8_Update;
            this.btnUpdate.Location = new System.Drawing.Point(341, 47);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 43);
            this.btnUpdate.TabIndex = 7;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // StaffInventoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(431, 322);
            this.ControlBox = false;
            this.Controls.Add(this.Logout);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.llbEnable);
            this.Controls.Add(this.txtPurchaseDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboSupplierName);
            this.Controls.Add(this.cboPurchaseOrderId);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(orderQuantityLabel);
            this.Controls.Add(this.orderQuantityNumericUpDown);
            this.Controls.Add(userNameLabel);
            this.Controls.Add(this.cboUserName);
            this.Controls.Add(productNameLabel);
            this.Controls.Add(this.cboProductName);
            this.Controls.Add(purchaseOrderIDLabel1);
            this.Controls.Add(purchaseOrderDateLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBack);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "StaffInventoryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StaffInventoryForm";
            this.Load += new System.EventHandler(this.StaffInventoryForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.orderQuantityNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.ComboBox cboProductName;
        private System.Windows.Forms.ComboBox cboUserName;
        private System.Windows.Forms.NumericUpDown orderQuantityNumericUpDown;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ComboBox cboPurchaseOrderId;
        private System.Windows.Forms.ComboBox cboSupplierName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPurchaseDate;
        private System.Windows.Forms.LinkLabel llbEnable;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Logout;
        private System.Windows.Forms.Button Exit;
    }
}