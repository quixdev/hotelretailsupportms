﻿using System;
using System.Windows.Forms;
using System.Data.OleDb;

namespace HotelRetailSupportMngment.v2
{
    public partial class StaffInventoryForm : Form
    {
        private OleDbConnection connect = new OleDbConnection();
        //ID variable used in Updating and Deleting Record  

        public StaffInventoryForm()
        {
            InitializeComponent();
            connect.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=E:\Documents\Visual Studio 2015\Projects\HotelRetailSupportMngment.v2\HotelRetailSupportMngment.v2\Resources\retailSupport.accdb";
            DisplayData();
        }
        //Insert Data  
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to Add Date?", "System Adding...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                if (cboSupplierName.Text == "" || cboUserName.Text == "" || txtPurchaseDate.Text == "" || orderQuantityNumericUpDown.Text == "" || cboProductName.Text == "")
                {
                    MessageBox.Show("Please fill in all the field");
                    return;
                }
                try
                {
                    //Query for add new data
                    connect.Open();
                    OleDbCommand command = new OleDbCommand();
                    command.Connection = connect;
                    command.CommandText = "insert into tbl_PurchaseOrder(productName,quantity,userName,supplierName,purchaseDate) values ('" + cboProductName.Text + "'," + orderQuantityNumericUpDown.Text + ",'" + cboUserName.Text + "','" + cboSupplierName.Text + "','" + txtPurchaseDate.Text + "')";
                    command.ExecuteNonQuery();
                    MessageBox.Show("Data Saved into tbl_PurchaseOrder");
                    connect.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("error" + ex);
                    connect.Close();
                }
                resetting();
            }          
        }
        //Display Data in DataGridView  
        private void DisplayData()
        {
            
        }
        //Clear Data  
        private void ClearData()
        {

        }
        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }
        //Update Record  
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to Save?", "System Saving...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                if (cboProductName.Text == "" || cboSupplierName.Text == "" || cboUserName.Text == "" || orderQuantityNumericUpDown.Text == "" || txtPurchaseDate.Text == "")
                {
                    MessageBox.Show("Please fill in all the field");
                    return;
                }
                else
                {
                    try
                    {
                        //Query For Edit data based on ProductName
                        connect.Open();
                        OleDbCommand command = new OleDbCommand();
                        command.Connection = connect;
                        string query = "update tbl_PurchaseOrder set productName ='" + cboProductName.Text + "' ,quantity =" + orderQuantityNumericUpDown.Text + " ,userName ='" + cboUserName.Text + "' ,supplierName ='" + cboSupplierName.Text + "',purchaseDate ='" + txtPurchaseDate.Text + "' where PurchaseID=" + cboPurchaseOrderId.Text + "";
                        MessageBox.Show(query);
                        command.CommandText = query;
                        command.ExecuteNonQuery();
                        MessageBox.Show("Data Updated/Edit & Saved into tbl_PurchaseOrder");
                        connect.Close();

                        btnAdd.Enabled = true;
                        btnDelete.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("error" + ex);
                        connect.Close();
                    }
                    resetting();
                }
            }                         
        }

        //Delete Record  
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to Delete?", "System Deleting...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                try
                {
                    //Query For Delete data by ProductName
                    connect.Open();
                    OleDbCommand command = new OleDbCommand();
                    command.Connection = connect;
                    string query = "delete from tbl_PurchaseOrder where PurchaseID=" + cboPurchaseOrderId.Text + "";
                    MessageBox.Show(query);
                    command.CommandText = query;
                    command.ExecuteNonQuery();
                    MessageBox.Show("Data Removed from tbl_PurchaseOrder");
                    connect.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error" + ex);
                    connect.Close();
                }
                resetting();
            }              
        }

        private void cboPurchaseOrderId_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            try
            {
                connect.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connect;
                string query = "select * from tbl_PurchaseOrder where PurchaseID=" + cboPurchaseOrderId.Text + "";
                command.CommandText = query;
                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {            
                    cboProductName.Text = reader["productName"].ToString();
                    cboUserName.Text = reader["userName"].ToString();
                    orderQuantityNumericUpDown.Text = reader["quantity"].ToString();
                    cboSupplierName.Text = reader["supplierName"].ToString();
                    cboPurchaseOrderId.Text = reader["PurchaseID"].ToString();
                    txtPurchaseDate.Text = reader["purchaseDate"].ToString();
                }
                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
                connect.Close();
            }
            btnAdd.Enabled = false;
        }

        private void StaffInventoryForm_Load(object sender, EventArgs e)
        {
            cboPurchaseOrderId.Enabled = false;
            btnUpdate.Enabled = false;
            using (OleDbCommand command = new OleDbCommand())
            {
                connect.Open();
                command.Connection = connect;
                string query = "Select * from tbl_PurchaseOrder";
                command.CommandText = query;
                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    cboPurchaseOrderId.Items.Add(reader["PurchaseID"].ToString());
                }
                connect.Close();
            }

            using (OleDbCommand command1 = new OleDbCommand())
            {
                connect.Open();
                command1.Connection = connect;
                string query = "Select * from tbl_Product";
                command1.CommandText = query;
                OleDbDataReader reader1 = command1.ExecuteReader();
                while (reader1.Read())
                {
                    cboProductName.Items.Add(reader1["productName"].ToString());
                }
                connect.Close();
            }
            using (OleDbCommand command2 = new OleDbCommand())
            {
                connect.Open();
                command2.Connection = connect;
                string query = "Select * from tbl_User";
                command2.CommandText = query;
                OleDbDataReader reader1 = command2.ExecuteReader();
                while (reader1.Read())
                {
                    cboUserName.Items.Add(reader1["userName"].ToString());
                }
                connect.Close();
            }
            using (OleDbCommand command3 = new OleDbCommand())
            {
                connect.Open();
                command3.Connection = connect;
                string query = "Select * from tbl_Supplier";
                command3.CommandText = query;
                OleDbDataReader reader1 = command3.ExecuteReader();
                while (reader1.Read())
                {
                    cboSupplierName.Items.Add(reader1["SupplierName"].ToString());
                }
                connect.Close();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
            StaffMenu staffmenu = new StaffMenu();
            staffmenu.ShowDialog();
        }

        private void llbEnable_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            cboPurchaseOrderId.Enabled = true;
            btnUpdate.Enabled = true;
        }
        public void resetting()
        {
            cboPurchaseOrderId.SelectedIndex = -1;
            txtPurchaseDate.Clear();
            cboProductName.SelectedIndex = -1;
            cboSupplierName.SelectedIndex = -1;
            cboUserName.SelectedIndex = -1;
            orderQuantityNumericUpDown.Value = 0;
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Logout_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginMenu newloginMenu = new LoginMenu();
            newloginMenu.Visible = true;
        }
    }
}
