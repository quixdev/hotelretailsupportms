﻿using System;
using System.Windows.Forms;
using System.Data.OleDb;


namespace HotelRetailSupportMngment.v2
{
    public partial class StaffSupplierForm : Form
    {
        public Form RefToFormStaffMenu { get; set; }
        private OleDbConnection connect = new OleDbConnection();
        public StaffSupplierForm()
        {
            InitializeComponent();
            connect.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=E:\Documents\Visual Studio 2015\Projects\HotelRetailSupportMngment.v2\HotelRetailSupportMngment.v2\Resources\retailSupport.accdb";            
        }
        
        private void btnBack_Click(object sender, EventArgs e)
        {
            this.RefToFormStaffMenu.Show();
            this.Close();

        }

        private void StaffSupplierForm_Load(object sender, EventArgs e)
        {
            using (OleDbCommand command = new OleDbCommand())
            {
                connect.Open();
                command.Connection = connect;
                string query = "Select * from tbl_Supplier";
                command.CommandText = query;
                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    cboSupplierName.Items.Add(reader["supplierName"].ToString());
                }
                connect.Close();
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {

            if (txtSupplierEmail.Text == "" || txtSupplierName.Text == "" || txtSupplierPhone.Text == "" )
            {
                MessageBox.Show("Please fill in all the field");
                return;
            }
            else
            {
                try
                {
                    //Query for add new data
                    connect.Open();
                    OleDbCommand command = new OleDbCommand();
                    command.Connection = connect;
                    command.CommandText = "insert into tbl_Supplier(SupplierName,supplierEmail,supplierPhone) values ('" + txtSupplierName.Text + "','" + txtSupplierEmail.Text + "','" + txtSupplierPhone.Text + "')";
                    command.ExecuteNonQuery();
                    MessageBox.Show("Data Saved into tbl_Supplier");
                    connect.Close();

                    //cboSearchProduct.Enabled = true;
                    resetting();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("error" + ex);
                    connect.Close();
                }
            }
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to Save?", "System saving...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                if (cboSupplierName.Text == "" || txtSupplierPhone.Text == "" || txtSupplierEmail.Text == "")
                {
                    MessageBox.Show("Please fill in all the field");
                    return;
                }
                else
                {
                    try
                    {
                        //Query For Edit data based on ProductName
                        connect.Open();
                        OleDbCommand command = new OleDbCommand();
                        command.Connection = connect;
                        string query = "update tbl_Supplier set SupplierName ='" + cboSupplierName.Text + "',supplierEmail ='" + txtSupplierEmail.Text + "' ,supplierPhone ='" + txtSupplierPhone.Text + "' where SupplierName='" + cboSupplierName.Text + "'";
                        MessageBox.Show(query);
                        command.CommandText = query;
                        command.ExecuteNonQuery();
                        MessageBox.Show("Data Updated/Edit & Saved into tbl_Supplier");
                        connect.Close();

                        //System Control Input
                        txtSupplierName.Enabled = true;
                        resetting();
                        btnAdd.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("error" + ex);
                        connect.Close();
                    }
                }
            }                         
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to Delete?", "System deleting...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                try
                {
                    //Query For Delete data by ProductName
                    connect.Open();
                    OleDbCommand command = new OleDbCommand();
                    command.Connection = connect;
                    string query = "delete from tbl_Supplier where supplierName='" + cboSupplierName.Text + "'";
                    MessageBox.Show(query);
                    command.CommandText = query;
                    command.ExecuteNonQuery();
                    MessageBox.Show("Data Removed from tbl_Supplier");
                    connect.Close();

                    //System Control Input
                    txtSupplierName.Enabled = true;
                    resetting();
                    btnAdd.Enabled = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error" + ex);
                    connect.Close();
                }
            }        
        }
     
        private void btnCancel_Click(object sender, EventArgs e)
        {
            txtSupplierEmail.Clear();
            txtSupplierName.Clear();
            txtSupplierPhone.Clear();
            cboSupplierName.SelectedIndex = -1;
            txtSupplierName.Enabled = true;
            btnAdd.Enabled = true;
        }
        public void resetting()
        {
            txtSupplierEmail.Clear();
            txtSupplierName.Clear();
            txtSupplierPhone.Clear();
            cboSupplierName.SelectedIndex = -1;
            txtSupplierName.Enabled = true;
            btnAdd.Enabled = true;
        }

        private void cboSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //Query For Edit data based on ProductName
                connect.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connect;
                string query = "select * from tbl_Supplier where SupplierName='" + cboSupplierName.Text + "'";
                command.CommandText = query;
                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    txtSupplierEmail.Text = reader["supplierEmail"].ToString();
                    txtSupplierName.Text = reader["SupplierName"].ToString();
                    txtSupplierPhone.Text = reader["supplierPhone"].ToString();
                    cboSupplierName.Text = reader["SupplierName"].ToString();
                }
                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
                connect.Close();
            }
            txtSupplierName.Enabled = false;
            btnAdd.Enabled = false;
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to exit?", "System Exit...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void Logout_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginMenu newloginMenu = new LoginMenu();
            newloginMenu.Visible = true;
        }
    }
}
