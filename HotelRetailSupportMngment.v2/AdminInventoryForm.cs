﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.OleDb;

namespace HotelRetailSupportMngment.v2
{
    public partial class PurchaseOrderInventory : Form
    {
        private OleDbConnection connect = new OleDbConnection();
        OleDbCommandBuilder builder = new OleDbCommandBuilder();
        OleDbDataAdapter adapter = new OleDbDataAdapter();
        DataTable table = new DataTable();
        public PurchaseOrderInventory()
        {
            InitializeComponent();
            connect.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=E:\Documents\Visual Studio 2015\Projects\HotelRetailSupportMngment.v2\HotelRetailSupportMngment.v2\Resources\retailSupport.accdb";
        }
        public Form RefToFormAdminMenu { get; set; }

        private void AdminInventoryForm_Load(object sender, EventArgs e)
        {
            try
            {
                //Query for add new data
                connect.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connect;
                string query = "select * from tbl_PurchaseOrder_Query";
                command.CommandText = query;

                OleDbDataAdapter dataAdapt = new OleDbDataAdapter(command);
                DataTable dataTable = new DataTable();
                dataAdapt.Fill(dataTable);
                datagridview1.DataSource = dataTable;

                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
                connect.Close();
            }
            
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.RefToFormAdminMenu.Show();
            this.Close();
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            this.Hide();
            ReportForm rprtForm = new ReportForm();
            rprtForm.ShowDialog();
        }


        private void BtnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to exit?", "System Exit...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void Logout_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginMenu newloginMenu = new LoginMenu();
            newloginMenu.Visible = true;
        }
    }
}
