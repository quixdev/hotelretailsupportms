﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.OleDb;

namespace HotelRetailSupportMngment.v2
{
    public partial class AdminUserForm : Form
    {
        private OleDbConnection connect = new OleDbConnection();
        public AdminUserForm()
        {
            InitializeComponent();
            connect.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=E:\Documents\Visual Studio 2015\Projects\HotelRetailSupportMngment.v2\HotelRetailSupportMngment.v2\Resources\retailSupport.accdb";
        }
        public Form RefToFormAdminMenu { get; set; }
        private void AdminUserForm_Load(object sender, EventArgs e)
        {
            try
            {
                //Query for add new data
                connect.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connect;
                string query = "select * from tbl_User";
                command.CommandText = query;

                OleDbDataAdapter dataAdapt = new OleDbDataAdapter(command);
                DataTable dataTable = new DataTable();
                dataAdapt.Fill(dataTable);
                datagridview1.DataSource = dataTable;

                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
                connect.Close();
            }

            using (OleDbCommand command = new OleDbCommand())
            {
                connect.Open();
                command.Connection = connect;
                string query = "Select * from tbl_User";
                command.CommandText = query;
                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    cboSearchUser.Items.Add(reader["UserID"].ToString());
                }
                connect.Close();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
            AdminMenu newAdminMenu = new AdminMenu();
            newAdminMenu.Show();
        }

        private void tbl_UserDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to add data?", "System adding...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                if (TxtPassword.Text == "" || TxtUserlevel.Text == "" || TxtUserName.Text == "")
                {
                    MessageBox.Show("Please fill in all the field");
                    return;
                }
                else
                {
                    try
                    {
                        //Query for add new data
                        connect.Open();
                        OleDbCommand command = new OleDbCommand();
                        command.Connection = connect;
                        command.CommandText = "insert into tbl_User(UserName,[Password],UserLevel) values ('" + TxtUserName.Text + "','" + TxtPassword.Text + "','" + TxtUserlevel.Text + "')";
                        command.ExecuteNonQuery();
                        MessageBox.Show("Data Saved into tbl_User");
                        connect.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("error" + ex);
                        connect.Close();
                    }
                }
            }
                
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to Save?", "System Saving...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                if (TxtUserName.Text == "" || TxtPassword.Text == "" || TxtUserlevel.Text == "")
                {
                    MessageBox.Show("Please fill in all the field");
                    return;
                }
                else
                {
                    try
                    {
                        connect.Open();
                        OleDbCommand command = new OleDbCommand();
                        command.Connection = connect;
                        string query = "update tbl_User set UserName ='" + TxtUserName.Text + "',[Password] ='" + TxtPassword.Text + "',UserLevel = '" + TxtUserlevel.Text + "' where UserID = " + lblUserID.Text + "";
                        MessageBox.Show(query);
                        command.CommandText = query;
                        command.ExecuteNonQuery();
                        MessageBox.Show("Data Updated/Edit & Saved into tbl_Product");
                        connect.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("error" + ex);
                        connect.Close();
                    }
                    resetting();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to Delete?", "System Deleting...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                try
                {
                    //Query For Delete data by ProductName
                    connect.Open();
                    OleDbCommand command = new OleDbCommand();
                    command.Connection = connect;
                    string query = "delete from tbl_User where UserID=" + lblUserID.Text + "";
                    MessageBox.Show(query);
                    command.CommandText = query;
                    command.ExecuteNonQuery();
                    MessageBox.Show("Data Removed from tbl_User");
                    connect.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error" + ex);
                    connect.Close();
                }
                resetting();
            }
                
        }

        private void cboSearchUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                connect.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connect;
                string query = "select * from tbl_User where UserID=" + cboSearchUser.Text + "";
                command.CommandText = query;
                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    lblUserID.Text = reader["UserID"].ToString();
                    TxtUserName.Text = reader["UserName"].ToString();
                    TxtPassword.Text = reader["Password"].ToString();
                    TxtUserlevel.Text = reader["UserLevel"].ToString();                    
                }
                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
                connect.Close();
            }
        }
        private void resetting ()
        {
            TxtUserName.Clear();
            TxtPassword.Clear();
            TxtUserlevel.SelectedIndex =- 1;
            cboSearchUser.SelectedIndex = -1;
        }

        private void AdminUserForm_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to exit?", "System Exit...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void Logout_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginMenu newloginMenu = new LoginMenu();
            newloginMenu.Visible = true;
        }
    }
}
