﻿using System;
using System.Windows.Forms;

namespace HotelRetailSupportMngment.v2
{
    public partial class StaffMenu : Form
    {
        public StaffMenu()
        {
            InitializeComponent();
        }
        public Form RefToFormStaffLogin { get; set; }
        private void btnInventory_Click(object sender, EventArgs e)
        {
            StaffInventoryForm staffInventory = new StaffInventoryForm();
            //staffInventory.RefToFormStaffMenu = this;
            this.Visible = false;
            staffInventory.Show();
        }

        private void btnProduct_Click(object sender, EventArgs e)
        {
            StaffProductForm staffProduct = new StaffProductForm();
            staffProduct.RefToFormStaffMenu = this;
            this.Visible = false;
            staffProduct.Show();
        }

        private void btnSupplier_Click(object sender, EventArgs e)
        {
            StaffSupplierForm staffSupplier = new StaffSupplierForm();
            staffSupplier.RefToFormStaffMenu = this;
            this.Visible = false;
            staffSupplier.Show();
        }

        private void btnUser_Click(object sender, EventArgs e)
        {
            StaffUserForm staffUser = new StaffUserForm();
            staffUser.RefToFormStaffMenu = this;
            this.Visible = false;
            staffUser.Show();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginMenu loginmenu = new LoginMenu();
            loginmenu.ShowDialog();
        }

        private void StaffMenu_Load(object sender, EventArgs e)
        {

        }

        private void Logout_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginMenu newloginMenu = new LoginMenu();
            newloginMenu.Visible = true;
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to exit?", "System Exit...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
