﻿using System;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data;
using System.Drawing;

namespace HotelRetailSupportMngment.v2
{
    public partial class ReportForm : Form
    {
        private OleDbConnection connect = new OleDbConnection();
        OleDbCommandBuilder builder = new OleDbCommandBuilder();
        OleDbDataAdapter adapter = new OleDbDataAdapter();
        DataTable table = new DataTable();
        public ReportForm()
        {
            InitializeComponent();
            connect.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=E:\Documents\Visual Studio 2015\Projects\HotelRetailSupportMngment.v2\HotelRetailSupportMngment.v2\Resources\retailSupport.accdb";
        }

        private void ReportForm_Load(object sender, EventArgs e)
        {
            TxtDate.Text= DateTime.Now.ToString("d/M/yyyy");
            string query = "SELECT SUM (totalPayment) from tbl_PurchaseOrder_Query";        
            using (OleDbCommand command1  = new OleDbCommand(query, connect))
            {
                connect.Open();
                object result = command1.ExecuteScalar();
                TxtPayment.Text = "RM "+ Convert.ToString(result);
                connect.Close();
            }

            using (OleDbCommand Command = new OleDbCommand(" SELECT count (PurchaseId) from tbl_PurchaseOrder as total", connect))
            {
                connect.Open();
                OleDbDataReader DB_Reader = Command.ExecuteReader();
                if (DB_Reader.HasRows)
                {
                    TxtUserName.Text = AdminLogin.userName;
                    DB_Reader.Read();
                    int id = DB_Reader.GetInt32(0);
                    TxtTotal.Text = Convert.ToString(id);
                }
                connect.Close();
            }
            using (OleDbCommand Command = new OleDbCommand(" SELECT count (ProductID) from tbl_Product as total", connect))
            {
                connect.Open();
                OleDbDataReader DB_Reader = Command.ExecuteReader();
                if (DB_Reader.HasRows)
                {
                    DB_Reader.Read();
                    int id = DB_Reader.GetInt32(0);
                    TxtTotal2.Text = Convert.ToString(id);
                }
                connect.Close();
            }
            using (OleDbCommand Command = new OleDbCommand(" SELECT count (SupplierID) from tbl_Supplier as total", connect))
            {
                connect.Open();
                OleDbDataReader DB_Reader = Command.ExecuteReader();
                if (DB_Reader.HasRows)
                {
                    DB_Reader.Read();
                    int id = DB_Reader.GetInt32(0);
                    TxtTotal3.Text = Convert.ToString(id);
                }
                connect.Close();
            }

            try
            {
                //Query for add new data
                connect.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connect;
                string query1 = "select * from tbl_PurchaseOrder_Query";
                command.CommandText = query1;

                OleDbDataAdapter dataAdapt = new OleDbDataAdapter(command);
                DataTable dataTable = new DataTable();
                dataAdapt.Fill(dataTable);
                dataGridView1.DataSource = dataTable;

                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
                connect.Close();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
            PurchaseOrderInventory newloginmenu = new PurchaseOrderInventory();
            newloginmenu.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to exit?", "System Exit...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void BtnPrint_Click(object sender, EventArgs e)
        {

        }

        private void Logout_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginMenu newloginMenu = new LoginMenu();
            newloginMenu.Visible = true;
        }
        
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.Graphics.DrawImage(bmp, 0, 0);
        }
        Bitmap bmp;
        private void button1_Click(object sender, EventArgs e)
        {
            Graphics gphc = this.CreateGraphics();
            bmp = new Bitmap(groupBox1.Width, groupBox1.Height, gphc);
            Graphics mg = Graphics.FromImage(bmp);
            mg.CopyFromScreen(groupBox1.PointToScreen(Point.Empty),Point.Empty,bmp.Size);
            printDocument1.DefaultPageSettings.Landscape = true;
            printPreviewDialog1.ShowDialog();

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void TxtUserName_Click(object sender, EventArgs e)
        {

        }
    }
}
