﻿using System;
using System.Windows.Forms;
using System.Data.OleDb;

namespace HotelRetailSupportMngment.v2
{
    public partial class StaffUserForm : Form
    {
        private OleDbConnection connect = new OleDbConnection();
        public Form RefToFormStaffMenu { get; set; }
        public StaffUserForm()
        {           
            InitializeComponent();
            connect.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=E:\Documents\Visual Studio 2015\Projects\HotelRetailSupportMngment.v2\HotelRetailSupportMngment.v2\Resources\retailSupport.accdb";
        }
        
        private void StaffUserForm_Load(object sender, EventArgs e)
        {
            txtUsername.Text = StaffLogin.userName;
            try
            {
                connect.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connect;
                
                string query = "select * from tbl_User where UserName='" + txtUsername.Text + "'";
                command.CommandText = query;
                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    txtUserId.Text = reader["UserID"].ToString();
                    txtUsername.Text = reader["UserName"].ToString();
                    txtPassword.Text = reader["Password"].ToString();
                    txtUserLevel.Text = reader["UserLevel"].ToString();
                                
                }
                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
                connect.Close();
            }

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.RefToFormStaffMenu.Show();
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to Save?", "System Saving...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                try
                {
                    connect.Open();
                    OleDbCommand command = new OleDbCommand();
                    command.Connection = connect;
                    string query = "update tbl_User set UserName ='" + txtUsername.Text + "',[Password] ='" + txtPassword.Text + "',UserLevel = '" + txtUserLevel.Text + "' where UserID=" + txtUserId.Text + "";
                    MessageBox.Show(query);
                    command.CommandText = query;
                    command.ExecuteNonQuery();
                    MessageBox.Show("Data Updated/Edit & Saved into tbl_PurchaseOrder");
                    connect.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error" + ex);
                    connect.Close();
                }
            }
               
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to exit?", "System Exit...",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void Logout_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginMenu newloginMenu = new LoginMenu();
            newloginMenu.Visible = true;
        }
    }
}
