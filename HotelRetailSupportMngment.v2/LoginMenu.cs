﻿using System;
using System.Windows.Forms;


namespace HotelRetailSupportMngment.v2
{
    public partial class LoginMenu : Form
    {
        public LoginMenu()
        {
            InitializeComponent();
        }
        

        private void btnAdmin_Click(object sender, EventArgs e)
        {
            AdminLogin adminlogin = new AdminLogin();
            adminlogin.RefToForm1 = this;
            this.Visible = false;
            adminlogin.Show();
        }

        private void btnStaff_Click(object sender, EventArgs e)
        {
            StaffLogin stafflogin = new StaffLogin();
            stafflogin.RefToForm1 = this;
            this.Visible = false;
            stafflogin.Show();
        }

        private void LoginMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void LoginMenu_Load(object sender, EventArgs e)
        {
        }
    }
}
